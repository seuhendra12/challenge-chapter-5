<h2>ERD</h2>
<img src="https://ik.imagekit.io/oaaw4tuek/Cars_Diagram_SquVAuD7l.png?ik-sdk-version=javascript-1.4.3&updatedAt=1665320602062">

<h2>Server</h2>
Http server  = http://localhost:3000/<br>

<h2>Endpoint</h2>
<h3>Endpoint API</h3>
<p>Create car : /api (POST)</p>
<p>Get car by id :  /api/:id (GET)</p>
<p>Update car, /api/:id/ (PUT)</p>
<p>Delete car :  /api/:id (DELETE)</p>
<p>Get Cars : /api (GET)</p>
<H3>Endpoint Page</h3>
<p>Show cars : /</p>
<p>Add new car : /tambah /</p>
<p>Edit car : /edit</p>
<p>Delete car : /hapus</p>
<h2>Pengunaan Request Body dan Respon Body</h2>
<h3>Request Body</h3>
<p>Contoh request untuk mengambil request search agar bisa di kirim ke server</p>
if (req.query.search) {
      const viewCars = await Cars.findAll({
        where: {
          nama: {
            [Op.or]: {
              [Op.iLike]: req.query.search,
              [Op.substring]: req.query.search,
            },
          },
        },
        });
      }
<h3>Respon Body</h3>
<p>merender halaman yang datanya ada ketika di search </p>
res.status(200).render("index", { layout: "parsials/layout", title: "Rental Mobil", viewCars, msg: req.flash("msg"), msgU: req.flash("msgU"), msgE: req.flash("msgE"), msgD: req.flash("msgD") });
